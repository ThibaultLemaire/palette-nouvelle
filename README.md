# Palette Nouvelle

Art Nouveau color palette. For use in themes.

This is currently a work in progress.

## Licenses

The work under this repository is [CC BY-NC 4.0] (see [LICENSE]).

The GIMP color palette [Art-Nouveau.gpl] has been derived from [the original work][Monica's post] of [Monica Francesca Petruzella] which is [CC BY-NC 4.0].

[CC BY-NC 4.0]: https://creativecommons.org/licenses/by-nc/4.0/deed.en_US
[LICENSE]: LICENSE
[Art-Nouveau.gpl]: Art-Nouveau.gpl
[Monica's post]: https://www.behance.net/gallery/20867567/Art-Nouveau-Color-Palette
[Monica Francesca Petruzella]: https://www.behance.net/Monica_Petruzzella
